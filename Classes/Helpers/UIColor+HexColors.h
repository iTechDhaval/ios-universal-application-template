//
//  UIColor+NFColors.h
//  UniversalExample
//
//  Created by Dhaval Patel on 29/09/12.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColors)

// Create a new UIColor instance using the following long value (see
// UIColor#colorWithHex for an example) and desired alpha value.
// 1.0 for opaque, 0.0 for transparent.
// Example: Light gray color of EFEFEF [UIColor colorWithHex:0xefefef alpha:1.0]
+ (UIColor *)colorWithHex:(long)hexColor alpha:(float)opacity;

@end
