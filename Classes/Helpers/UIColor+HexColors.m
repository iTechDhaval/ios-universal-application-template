//
//  UIColor+NFColors.m
//  UniversalExample
//
//  Created by Dhaval Patel on 29/09/12.
//
//

#import "UIColor+HexColors.h"

@implementation UIColor (HexColors)

+ (UIColor *)colorWithHex:(long)hexColor alpha:(float)opacity
{
    float red = ((float)((hexColor & 0xFF0000) >> 16))/255.0;
    float green = ((float)((hexColor & 0xFF00) >> 8))/255.0;
    float blue = ((float)(hexColor & 0xFF))/255.0;
    return [UIColor colorWithRed:red green:green blue:blue alpha:opacity];
}

@end
